/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bjacob.activity.web.rest.vm;

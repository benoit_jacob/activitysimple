import { ITask } from 'app/shared/model//task.model';
import { IEmployee } from 'app/shared/model//employee.model';

export interface IActivity {
    id?: number;
    name?: string;
    description?: string;
    tasks?: ITask[];
    employee?: IEmployee;
}

export class Activity implements IActivity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public tasks?: ITask[],
        public employee?: IEmployee
    ) {}
}

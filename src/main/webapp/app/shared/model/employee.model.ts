import { Moment } from 'moment';
import { IActivity } from 'app/shared/model//activity.model';

export interface IEmployee {
    id?: number;
    name?: string;
    lastName?: string;
    birthDate?: Moment;
    stateProvince?: string;
    activities?: IActivity[];
}

export class Employee implements IEmployee {
    constructor(
        public id?: number,
        public name?: string,
        public lastName?: string,
        public birthDate?: Moment,
        public stateProvince?: string,
        public activities?: IActivity[]
    ) {}
}

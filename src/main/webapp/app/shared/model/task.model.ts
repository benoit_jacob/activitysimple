import { IActivity } from 'app/shared/model//activity.model';

export interface ITask {
    id?: number;
    name?: string;
    description?: string;
    estimatedTime?: number;
    activity?: IActivity;
}

export class Task implements ITask {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public estimatedTime?: number,
        public activity?: IActivity
    ) {}
}

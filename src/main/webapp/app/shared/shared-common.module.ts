import { NgModule } from '@angular/core';

import { ActivitySimpleSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [ActivitySimpleSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [ActivitySimpleSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class ActivitySimpleSharedCommonModule {}
